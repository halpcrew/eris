import os
import json
import socket
import sys
import ctypes
import logging
import inspect
from network.networkmanager import Cloud
from main import Custom_ValidationError, extra_validate#,IPValidator
file_name = 'data.json'

logger = logging.getLogger('Eris')

class MigrationRequired(Exception):
    pass


class ConfigData:
    instance = None

    class __DataSource:
        def __init__(self, data_file):
            self.data_file = data_file
            self.data = {
                'config': {},
                'debug': False,
                'token': None
            }
            if not os.path.isfile(data_file):
                self.__create()
            else:
                with open(self.data_file, "r") as file:
                    self.data = json.load(file)

            self.data['token'] = self.data.get('token', None)
            self.data['debug'] = self.data.get('debug', False)

        def save(self):
            if not os.path.isfile(self.data_file):
                self.__create()
            with open(self.data_file, "w") as file:
                json.dump(self.data, file, indent=4)
                logger.debug('Dumped {} into config.'.format(self.data))

        def __create(self):
            with open(self.data_file, "w") as write_file:
                json.dump(self.data, write_file, indent=4)

    def __init__(self, data_file):
        if not ConfigData.instance:
            ConfigData.instance = ConfigData.__DataSource(data_file)

    def get(self, key, default=None):
        logger.debug('get {}'.format(key))
        return self.instance.data.get(key, default)

    def set(self, key, value):
        logger.debug('set {} as {}'.format(key, value))
        self.instance.data[key] = value

    def save(self):
        curframe = inspect.currentframe()
        calframe = inspect.getouterframes(curframe, 2)
        if self.instance:
            self.instance.save()
            logger.debug('Config is {}'.format(self.instance.data))
            logger.debug('saved config with caller {}'.format(calframe[1][3]))

    def __iter__(self):
        return iter(self.instance.data.items()) if self.instance else iter({}.items())


class CustomList(ConfigData):
    def __init__(self, name):
        super().__init__(None)
        self.name = name
        self.data = self.instance.data.get(name, None) if self.instance else None
        
        #self.data = self.instance.data.get(name, None) if self.instance else None
        if type(self.data) is list:
            raise MigrationRequired("Need to update to using dicts")
        if self.data is None:
            self.data = {}
            self.instance.data[self.name] = self.data
            self.save()

    def __contains__(self, key):
        return key in self.data

    def __iter__(self):
        return iter(self.data.items())

    def __len__(self):
        return len(self.data)

    def __str__(self):
        return str(self.data)

    def add(self, key, value):
        self.data[key] = value
        logger.debug('Added key: {} added value: {} data[key] is: {}'.format(key, value, self.data[key]))
        logger.debug('Self.data is {}'.format(self.data))
        logger.debug('instance.data is {}'.format(self.instance.data))

    def get(self, key, default=None):
        return self.data.get(key, default)

    def has(self, value, key='name'):
        """
        Check if item exists from the list by it's key property value
        :param value:
        :param key:
        :return:
        """
        items = self.find_all(value, key)
        return True if items else False

    def find(self, value, key='name'):
        items = self.find_all(value, key)
        return items[0] if items else (None, None)

    def find_all(self, value, key='name'):
        return [(k, val) for k, val in self.data.items() if val.get(key, None) == value]

    def pop(self, key, default=None):
        return self.data.pop(key, default)

    def delete(self, key):
        self.pop(key, None)


def update_cloud_friends():
    config = ConfigData(file_name)
    friends = CustomList('friends')
    token = config.get('token', None)
    runner = Cloud(token)
    cloud_friends_list = runner.get_friends()
    for friend in cloud_friends_list:
        logger.debug(friend)
        logger.debug(friends.find(friend.get('name')))
        ip, f = friends.find(friend.get('name'))
        friend_item = {'name': friend.get('name'), 'enabled': False}
        if f:
            friend_item = f
            friends.delete(ip)
        friends.add(friend.get('ip'), friend_item)
    missing = list()
    for key, friend in friends.data.items():
        if not any(cloud_friend.get('name') == friend.get('name') for cloud_friend in cloud_friends_list):
            missing.append(key)
    for key in missing:
        friends.delete(key)
    config.save()
    return friends


def migrate_to_dict():
    error = False
    config = ConfigData(file_name)
    for key, value in config:
        if type(value) is list:
            d = {}
            for item in value:
                ip = item.pop('ip')
                ip_calc = extra_validate.validate_ip(ip)
                if ip_calc != False:
                    if ip != ip_calc:
                        item['value'] = ip
                    d[ip_calc] = item
                else:
                    if not sys.platform == 'darwin':
                        reps = ctypes.windll.user32.MessageBoxW(0, "Some entrys are not correct, do you want to remove them?.", "Migrating Config", 4)
                    else:
                        reps = 6
                    if reps == 7:
                        error = True
                    continue
            config.set(key, d)
    if not error:
        config.save()
        ctypes.windll.user32.MessageBoxW(0, "Config files required migration, please restart the program.", "Migrating Config", 0)
        logger.info("Config files required migration, please restart the program.")
    else:
        ctypes.windll.user32.MessageBoxW(0, "Issues found in some items, delete or fix them manually to proceed.", "Migrating Config", 0)
        logger.info("Issues found in some items, delete or fix them manually to proceed.")
