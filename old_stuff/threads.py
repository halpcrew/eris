from PyQt5 import uic, QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import *
from network import networkmanager
import time, logging, sys, socket, data
import validators
#from main import Custom_ValidationError, extra_validate

logger = logging.getLogger('Eris')

if sys.platform == 'darwin':
    from network.blocker_printonly import *
    logger.warning('Eris is running on Mac, it will simulate the functions but will not actually block anything')
else:
    from network.blocker import *

def get_public_ip():
    public_ip = networkmanager.Cloud().get_ip()
    if public_ip:
        logger.info('Got a public IP')
        return public_ip
    else:
        logger.warning('Failed to get public IP')
        return False


def get_private_ip():
    soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    soc.connect(("8.8.8.8", 80))
    local_ip = soc.getsockname()[0]
    soc.close()
    return local_ip

class thread_Solo(QtCore.QThread):

    started = QtCore.pyqtSignal()
    def __init__(self):
        QtCore.QThread.__init__(self)
        
 
    def __del__(self):
        logger.debug('solo thread closing down')
        self.doContinue = False
        self.wait()

    def stop(self):
        logger.debug('solo thread closing down')
        self.doContinue = False
        self.wait()


    def run(self):
        self.doContinue = True
        self.started.emit()
        try:
            while self.doContinue:
                whitelist = Whitelist(ips=[])
                whitelist.start()
                logger.debug("solo running")
                for _ in range(10):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
                whitelist.stop()
                for _ in range(15):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
        finally:
            whitelist.stop()
            logger.debug("stop")
        logger.debug("thread is done")

class thread_Whitelist(QtCore.QThread):

    started = QtCore.pyqtSignal()
    def __init__(self):
        QtCore.QThread.__init__(self)
        
 
    def __del__(self):
        logger.debug('Whitelist thread closing down')
        self.doContinue = False
        self.wait()

    def stop(self):
        logger.debug('Whitelist thread closing down')
        self.doContinue = False
        self.wait()


    def run(self):
        #blacklist = data.CustomList('blacklist')
        custom_ips = data.CustomList('custom_ips')
        friends = data.CustomList('friends')
        self.doContinue = True
        local_ip = get_private_ip()
        ip_set = {local_ip}
        public_ip = get_public_ip()
        if public_ip:
            ip_set.add(public_ip)
        else:
            logger.warning('Failed to get Public IP. Running without.')

        for ip, friend in custom_ips:
            if friend.get('enabled'):
                try:
                    ip_calc = validate_ip(ip)
                    ip_set.add(ip_calc)
                except Custom_ValidationError:
                    logger.warning('Not valid IP or URL: {}'.format(ip))
                    continue

        for ip, friend in friends:
            if friend.get('enabled'):
                ip_set.add(ip)

        logger.info('Starting whitelisted session with {} IPs'.format(len(ip_set)))
        
        self.started.emit()
        try:
            while self.doContinue:
                packet_filter = Whitelist(ips=ip_set)
                packet_filter.start()
                for _ in range(10):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
                packet_filter.stop()
                for _ in range(15):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
        finally:
            logger.debug("stop")
        logger.debug("thread is done")

class thread_Blacklist(QtCore.QThread):

    started = QtCore.pyqtSignal()
    def __init__(self):
        QtCore.QThread.__init__(self)
        
 
    def __del__(self):
        logger.debug('Blacklist thread closing down')
        self.doContinue = False
        self.wait()

    def stop(self):
        logger.debug('Blacklist thread closing down')
        self.doContinue = False
        self.wait()


    def run(self):
        blacklist = data.CustomList('blacklist')
        self.doContinue = True
        ip_set = set()
        for ip, item in blacklist:
            if item.get('enabled'):
                try:
                    ip = validate_ip(item.get('ip'))
                    ip_set.add(ip)
                except Custom_ValidationError:
                    logger.warning('Not valid IP or URL: {}'.format(ip))
                    continue
        logger.info('Starting blacklisted session with {} IPs'.format(len(ip_set)))

        
        self.started.emit()
        try:
            while self.doContinue:
                packet_filter = Blacklist(ips=ip_set)
                packet_filter.start()
                for _ in range(10):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
                packet_filter.stop()
                for _ in range(15):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
        finally:
            logger.debug("stop")
        logger.debug("thread is done")