import re, ipaddress, data, socket, time

ipv4 = re.compile(r"((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}")
domain = re.compile(r"^[a-z]+([a-z0-9-]*[a-z0-9]+)?(\.([a-z]+([a-z0-9-]*[\[a-z0-9]+)?)+)*$")

config = data.ConfigData(data.file_name)
try:
    blacklist = data.CustomList('blacklist')
    custom_ips = data.CustomList('custom_ips')
    friends = data.CustomList('friends')
except data.MigrationRequired:
    data.migrate_to_dict()
    time.sleep(5)
    exit(0)

class Custom_ValidationError(Exception):
   """Base class for other exceptions"""
   pass
class extra_validate():
    @staticmethod
    def validate_ip(text):
        error = Custom_ValidationError()#message='Not a valid IP or URL')
        try:
            ip = text
            if ipv4.match(ip):
                ipaddress.IPv4Address(ip)
            elif domain.match(ip):
                ip = socket.gethostbyname(text)
                ipaddress.IPv4Address(ip)
            else:
                raise error
            return ip
        except (ipaddress.AddressValueError, socket.gaierror):
            raise error

    @staticmethod
    def name_in_custom(name):
        if custom_ips.find(name):
            raise Custom_ValidationError
        return name

    @staticmethod
    def ip_in_custom(ip):
        if ip in custom_ips or custom_ips.find(ip, 'value'):
            raise Custom_ValidationError
        return ip
        
    @staticmethod
    def name_in_blacklist(name):
        if blacklist.find(name):
            raise Custom_ValidationError
        return name

    @staticmethod
    def ip_in_blacklist(ip):
        if ip in blacklist or blacklist.find(ip, 'value'):
            raise Custom_ValidationError
        return ip
