astroid==2.3.3
certifi==2019.11.28
chardet==3.0.4
colorama==0.4.3
cx-Freeze==6.1
idna==2.9
isort==4.3.21
lazy-object-proxy==1.4.3
mccabe==0.6.1
packaging==20.3
pydivert==2.1.0
pylint==2.4.4
pyparsing==2.4.6
PyQt5==5.14.1
PyQt5-sip==12.7.1
requests==2.23.0
six==1.14.0
urllib3==1.25.8
wrapt==1.11.2