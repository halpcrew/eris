version = '1.0.0'
window_title = 'Eris | Version {}'.format(version)

from PyQt5 import uic, QtWidgets, QtCore, QtGui, Qt
from PyQt5.QtWidgets import * #QWidget, QMessageBox, QInputDialog, QLineEdit, QVBoxLayout, QPushButton, QGroupBox, QListWidget, QCheckBox
import logging, sys

logger = logging.getLogger('Eris')
logger.propagate = False
log_level = logging.INFO
logger.setLevel(log_level)
if not logger.handlers:
    fh = logging.FileHandler(filename='history.log')
    fh.setLevel(log_level)
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(log_level)
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

from functools import partial
from network import networkmanager
#from network.blocker import IPSyncer
from multiprocessing import freeze_support, Event
from packaging.version import parse as parse_version
import data, ipaddress, socket, ctypes, re, time, webbrowser
#from threads import thread_Solo, thread_Whitelist


#define for what is a ip and what is a domain
ipv4 = re.compile(r"((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}")
domain = re.compile(r"^[a-z]+([a-z0-9-]*[a-z0-9]+)?(\.([a-z]+([a-z0-9-]*[\[a-z0-9]+)?)+)*$")

#call to network manager to have correct version in useragent
networkmanager.Cloud().set_version(version)

# Custom exeption
class Custom_ValidationError(Exception):
   """Base class for other exceptions"""
   pass
class extra_validate():
    #validate ips thru out the app
    @staticmethod
    def validate_ip(text):
        error = Custom_ValidationError()#message='Not a valid IP or URL')
        try:
            ip = text
            #does the inputed text match ipv4 defined earlier
            if ipv4.match(ip):
                ipaddress.IPv4Address(ip)
            #or does the inputed text match domain defined earlier
            elif domain.match(ip):
                ip = socket.gethostbyname(text)
                ipaddress.IPv4Address(ip)
            else:
                return False
            return ip
        except (ipaddress.AddressValueError, socket.gaierror):
            return False

    #search thru customips for the inputed name
    @staticmethod
    def name_in_custom(name):
        custom_ips = data.CustomList('custom_ips')
        if custom_ips.has(name):
            return False
        return name

    #search trhu customips for the inputed ip
    @staticmethod
    def ip_in_custom(ip):
        custom_ips = data.CustomList('custom_ips')
        if ip in custom_ips or custom_ips.has(ip, 'value'):
            return False
        return ip
    
    @staticmethod
    def name_in_blacklist(name):
        blacklist = data.CustomList('blacklist')
        if blacklist.has(name):
            return False
        return name

    @staticmethod
    def ip_in_blacklist(ip):
        blacklist = data.CustomList('blacklist')
        if ip in blacklist or blacklist.has(ip, 'value'):
            return False
        return ip

if sys.platform == 'darwin':
    from network.blocker_printonly import *
    logger.warning('Eris is running on Mac, it will simulate the functions but will not actually block anything')
else:
    from network.blocker import *
class extra_template():
    def reset(self, temp):
        temp.findChild(QtWidgets.QPushButton, 'btn1').hide()
        temp.findChild(QtWidgets.QPushButton, 'btn2').hide()
        temp.findChild(QtWidgets.QPushButton, 'btn3').hide()
        temp.findChild(QtWidgets.QPushButton, 'btn4').hide()
        temp.findChild(QtWidgets.QPushButton, 'btn5').hide()
        temp.findChild(QtWidgets.QPushButton, 'btn6').hide()
        temp.findChild(QtWidgets.QPushButton, 'btn7').hide()
        temp.findChild(QtWidgets.QPushButton, 'btn8').hide()
        temp.findChild(QtWidgets.QLabel, 'txttop').hide()

class extra_tokencheck():
    @staticmethod
    def check(token, strmode):
        conn = networkmanager.Cloud(token)
        logger.debug('Token is {}'.format(token))
        if token == None and strmode:
            logger.warning('No token entered')
            return 'No token entered'
        if strmode:
            if conn.check_connection():
                if conn.check_token():
                    logger.info('Token is good and Cloud is up')
                    return 'Token is good and Cloud is up'
                else:
                    logger.error('Token is bad but Cloud is up')
                    return 'Token is bad but Cloud is up'
            else:
                logger.error('Cloud is down')
                return 'Cloud is down'
        else:
            if conn.check_connection():
                if conn.check_token():
                    logger.info('Token is good and Cloud is up')
                    return True
                else:
                    logger.error('Token is bad but Cloud is up')
                    return False
            else:
                logger.error('Cloud is down')
                return False
class Base(QMainWindow):
    def __init__(self):
        super(Base, self).__init__()
        
        #ipsync()
        
        #token = data.read_file().get('token')
        #if token:
        #    global status_txt
        #    status_txt = token_check(token)
        self.settings = QtCore.QSettings('HALP Crew', 'Eris')
        #global geometry
        geometry = self.settings.value('geometry', bytes('', 'utf-8'))
        if sys.platform == 'darwin':
            QMessageBox.warning(self, 'Running on mac', "Blocking packets is not supported on mac, Eris will only simulate the functions", QMessageBox.Ignore)
        
        cloud = networkmanager.Cloud(token)
        temp_friends = cloud.get_all()
        global red_mode
        if temp_friends:
            if {'name': '04heja'} in temp_friends:
                logger.info('Hello Red :)')
                
                red_mode = True
                ctypes.windll.user32.MessageBoxW(0, "Im soooo sorry Red :( But it had to be done.", "RIP Red", 0)
            else: red_mode = False
        else: red_mode = False
        self.menu_main = menu_MainPage(geometry)
        self.menu_main.show()
        
    @staticmethod
    def default_stuff(self):
        
        self.setWindowTitle(window_title)
        self.findChild(QtWidgets.QPushButton, 'help').clicked.connect(Base.open)
        if red_mode:
            self.findChild(QtWidgets.QLabel, 'label_2').setStyleSheet('color: #2e2e2e; line-height: 1.10;')
            self.findChild(QtWidgets.QLabel, 'label_2').setText('The My Little Pony Game')
            self.findChild(QtWidgets.QLabel, 'label').setPixmap(QtGui.QPixmap('img/red.png'))
            self.findChild(QtWidgets.QLabel, 'txtbtm').setStyleSheet('color: #2e2e2e; line-height: 1.10;')
            self.findChild(QtWidgets.QWidget, 'centralwidget').setStyleSheet('background-color: #F5B7D0')
            self.setWindowTitle(window_title.replace('Eris','My Little Pony Halper'))

    
    def open(self):
        about_menu = menu_About()
        about_menu.show()
        #self.close()
        

class menu_About(QWidget):
    def __init__(self):
        super(menu_About, self).__init__()

        uic.loadUi('ui/about.ui', self)
        self.setWindowTitle('About')
        self.findChild(QtWidgets.QLabel, 'Icon').mousePressEvent = self.dosomething
        self.show()
    def dosomething(self, event):
        self.close

class menu_MainPage(QWidget):
    def __init__(self, geometry):
        super(menu_MainPage, self).__init__()

        uic.loadUi('ui/main.ui', self)
        Base.default_stuff(self)

        self.findChild(QtWidgets.QPushButton, 'solo').clicked.connect(partial(self.btn_func, 'solo'))
        self.findChild(QtWidgets.QPushButton, 'whitelist').clicked.connect(partial(self.btn_func, 'whitelist'))
        self.findChild(QtWidgets.QPushButton, 'blacklist').clicked.connect(partial(self.btn_func, 'blacklist'))
        self.findChild(QtWidgets.QPushButton, 'autowhitelist').clicked.connect(partial(self.btn_func, 'autowhitelist'))
        self.findChild(QtWidgets.QPushButton, 'newsession').clicked.connect(partial(self.btn_func, 'newsession'))
        self.findChild(QtWidgets.QPushButton, 'kick').clicked.connect(partial(self.btn_func, 'kick'))
        self.findChild(QtWidgets.QPushButton, 'settings').clicked.connect(partial(self.btn_func, 'settings'))
        self.findChild(QtWidgets.QPushButton, 'quit').clicked.connect(partial(self.btn_func, 'quit'))
        #self.findChild(QtWidgets.QLabel, 'txtbtm').clicked.connect(partial(self.btn_func, 'quit'))
        
        self.txtbtm = self.findChild(QtWidgets.QLabel, 'txtbtm')
        self.findChild(QtWidgets.QLabel, 'label').mousePressEvent = self.secret
        

        #Check token and cloud
        self.token_status = extra_tokencheck.check(config.get('token'), True)
        
        if self.token_status == 'Token is good and Cloud is up':
            self.status_txt = '<font>Main Menu</font><font color=\"lime\"><br>Token is good and Cloud is up</font>'
        elif self.token_status == 'Token is bad but Cloud is up':
            self.status_txt = '<font>Main Menu</font><font color=\"yellow\"><br>Token is bad but Cloud is up</font>'
        elif self.token_status == 'No token entered':
            self.status_txt = '<font>Main Menu</font><font color=\"red\"><br>No token entered</font>'
        else:
            self.status_txt = '<font>Main Menu</font><font color=\"red\"><br>Cloud is down</font>'
        self.txtbtm.setText(self.status_txt)
        self.secret_time_clicked = 0
        if red_mode: self.redmode()
        self.restoreGeometry(geometry)
        self.show()
    def secret(self, event):
        self.secret_time_clicked = self.secret_time_clicked + 1
        if self.secret_time_clicked > 4:
            self.secret_time_clicked = 0
            self.findChild(QtWidgets.QLabel, 'label').setPixmap(QtGui.QPixmap('img/secret.png'))
            self.findChild(QtWidgets.QLabel, 'label_2').setText("<font size=\"2\">RaWr | JUST EAT THEM HACKERS!</font>")
            self.findChild(QtWidgets.QPushButton, 'solo').setText("Alone PLZ")
            self.findChild(QtWidgets.QPushButton, 'whitelist').setText("Rawers only")
            self.findChild(QtWidgets.QPushButton, 'settings').setText("dant tauch me")
            app.setWindowIcon(QtGui.QIcon('img/secret.png'))

    def redmode(self):
        self.findChild(QtWidgets.QPushButton, 'solo').setText("Cowpony")
        self.findChild(QtWidgets.QPushButton, 'whitelist').setText("Pin the Tail\n on the Pony")
        self.findChild(QtWidgets.QPushButton, 'settings').setText("Pony Settings")
        self.findChild(QtWidgets.QPushButton, 'newsession').setText("BE FREE PONY!")
        self.findChild(QtWidgets.QPushButton, 'autowhitelist').setText("PoNY Like\n People heRE!")
        self.findChild(QtWidgets.QPushButton, 'blacklist').setText("HACKERS!")
        self.findChild(QtWidgets.QPushButton, 'kick').setText("OwO Red!")
        self.findChild(QtWidgets.QPushButton, 'quit').setText("Dont leave OwO")
        app.setWindowIcon(QtGui.QIcon('img/red.png'))

    def btn_func(self, btn):
        # This is executed when the button is pressed
        if btn == "quit":
            #self.wl_thread.__del__()
            self.close()
        elif btn == "solo":
            geometry = self.saveGeometry()
            self.menu = menu_Solo(geometry)
            self.menu.show()
            self.close()
            #self.wl_thread.__del__()

        elif btn == "whitelist":
            geometry = self.saveGeometry()
            self.menu = menu_Whitelist(geometry)
            self.menu.show()
            self.close()

        elif btn == "autowhitelist":
            geometry = self.saveGeometry()
            self.menu = menu_AutoWhitelist(geometry)
            self.menu.show()
            self.close()

        elif btn == "kick":
            geometry = self.saveGeometry()
            self.menu = menu_Kick(geometry)
            self.menu.show()
            self.close()

        elif btn == "newsession":
            
            logger.info('Creating new session')
            #time.sleep(1)
            time.sleep(1)
            packet_filter = Whitelist(ips=[])
            packet_filter.start()
            #self.txtbtm.setText('Creating new session')
            for _ in range(10):
                self.txtbtm.setText('Creating new session')
                time.sleep(1)
            packet_filter.stop()
            QMessageBox.information(self, 'New session', "New session created, have fun :)", QMessageBox.Ok)
            self.txtbtm.setText(self.status_txt)

        elif btn == "blacklist":
            geometry = self.saveGeometry()
            self.menu = menu_Blacklist(geometry)
            self.menu.show()
            self.close()


        elif btn == "settings":
            geometry = self.saveGeometry()
            self.menu = menu_settings(geometry)
            self.menu.show()
            self.close()

# Solo menu
class menu_Solo(QWidget):
    def __init__(self, geometry):
        super(menu_Solo, self).__init__()
        uic.loadUi('ui/template.ui', self)
        Base.default_stuff(self)
        extra_template.reset(self, self)

        self.btn_stop = self.findChild(QtWidgets.QPushButton, 'btn1')
        self.txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')
        self.txt_top = self.findChild(QtWidgets.QLabel, 'txttop')

        self.btn_stop.show()
        self.txt_btm.show()
        self.txt_top.show()

        # adding by emitting signal in different thread
        self.solo_thread = thread_Solo()
        self.solo_thread.start()
        self.solo_thread.started.connect(self.solo_started)
        
        # Define function to do on clicks
        self.btn_stop.clicked.connect(self.stop)

        self.btn_stop.setText('Stop')
        self.txt_btm.setText('Solo Session')

        self.txt_top.setText('Starting Solo')
        self.txt_top.setStyleSheet('color: yellow')
        
        self.restoreGeometry(geometry)

    def solo_started(self):
        self.txt_top.setText('Solo Started')
        self.txt_top.setStyleSheet('color: lime')
    
    def stop(self):
        geometry = self.saveGeometry()
        self.txt_top.setText('Stoping Solo')
        self.txt_top.setStyleSheet('color: red')
        self.solo_thread.stop()
        self.menu_mainmenu = menu_MainPage(geometry)
        self.menu_mainmenu.show()
        self.close()
# Solo menu
class menu_Blacklist(QWidget):
    def __init__(self, geometry):
        super(menu_Blacklist, self).__init__()
        uic.loadUi('ui/template.ui', self)
        Base.default_stuff(self)
        extra_template.reset(self, self)

        self.btn_stop = self.findChild(QtWidgets.QPushButton, 'btn1')
        self.txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')
        self.txt_top = self.findChild(QtWidgets.QLabel, 'txttop')

        self.btn_stop.show()
        self.txt_btm.show()
        self.txt_top.show()

        # adding by emitting signal in different thread
        self.thread = thread_Blacklist()
        self.thread.start()
        self.thread.started.connect(self.solo_started)
        
        # Define function to do on clicks
        self.btn_stop.clicked.connect(self.stop)

        self.btn_stop.setText('Stop')
        self.txt_btm.setText('Blacklist Session')

        self.txt_top.setText('Starting Blacklist')
        self.txt_top.setStyleSheet('color: yellow')
        
        self.restoreGeometry(geometry)

    def solo_started(self):
        self.txt_top.setText('Blacklist Started')
        self.txt_top.setStyleSheet('color: lime')
    
    def stop(self):
        geometry = self.saveGeometry()
        self.txt_top.setText('Stoping Blacklist')
        self.txt_top.setStyleSheet('color: red')
        self.thread.stop()
        self.menu_mainmenu = menu_MainPage(geometry)
        self.menu_mainmenu.show()
        self.close()
       
# Whitelist menu
class menu_Whitelist(QWidget):
    def __init__(self, geometry):
        super(menu_Whitelist, self).__init__()
        uic.loadUi('ui/template.ui', self)
        Base.default_stuff(self)
        extra_template.reset(self, self)

        self.btn_stop = self.findChild(QtWidgets.QPushButton, 'btn1')
        self.txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')
        self.txt_top = self.findChild(QtWidgets.QLabel, 'txttop')

        self.btn_stop.show()
        self.txt_btm.show()
        self.txt_top.show()

        # adding by emitting signal in different thread
        self.wl_thread = thread_Whitelist()
        self.wl_thread.start()
        self.wl_thread.started.connect(self.whitelist_started)
        
        # Define function to do on clicks
        self.btn_stop.clicked.connect(self.stop)

        self.btn_stop.setText('Stop')
        self.txt_btm.setText('Whitelist Session')

        self.txt_top.setText('Starting Whitelist')
        self.txt_top.setStyleSheet('color: yellow')
        
        self.restoreGeometry(geometry)

    def whitelist_started(self):
        self.txt_top.setText('Whitelist Started')
        self.txt_top.setStyleSheet('color: lime')
    
    def stop(self):
        geometry = self.saveGeometry()
        self.txt_top.setText('Stoping Solo')
        self.txt_top.setStyleSheet('color: red')
        self.wl_thread.stop()
        self.menu_mainmenu = menu_MainPage(geometry)
        self.menu_mainmenu.show()
        self.close()

# Auto Whitelist menu
class menu_AutoWhitelist(QWidget):
    def __init__(self, geometry):
        super(menu_AutoWhitelist, self).__init__()
        uic.loadUi('ui/template.ui', self)
        Base.default_stuff(self)
        extra_template.reset(self, self)

        self.btn_stop = self.findChild(QtWidgets.QPushButton, 'btn1')
        self.txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')
        self.txt_top = self.findChild(QtWidgets.QLabel, 'txttop')

        self.btn_stop.show()
        self.txt_btm.show()
        self.txt_top.show()

        # adding by emitting signal in different thread
        self.thread = thread_AutoWhitelist(self.txt_top)
        self.thread.start()
        self.thread.started.connect(self.whitelist_started)
        
        # Define function to do on clicks
        self.btn_stop.clicked.connect(self.stop)
        self.findChild(QtWidgets.QPushButton, 'help').clicked.connect(self.helpbtn)

        self.btn_stop.setText('Stop')
        self.txt_btm.setText('Auto Whitelist Session')

        self.txt_top.setText('Collecting IPs')
        self.txt_top.setStyleSheet('color: yellow')
        time.sleep(1)
        self.txt_top.setText('Starting Auto Whitelist')
        
        self.restoreGeometry(geometry)

    def whitelist_started(self):
        self.txt_top.setStyleSheet('color: lime')
        self.txt_top.setText('IPs Collected')
        time.sleep(1.5)
        self.txt_top.setText('Whitelist Started')
    
    def helpbtn(self):
        logger.debug(self.thread.found_ips)
    
    def stop(self):
        geometry = self.saveGeometry()
        self.txt_top.setText('Stoping Solo')
        self.txt_top.setStyleSheet('color: red')
        self.thread.stop()
        self.menu_mainmenu = menu_MainPage(geometry)
        self.menu_mainmenu.show()
        self.close()

# Kick menu
class menu_Kick(QWidget):
    def __init__(self, geometry):
        self.geometry = geometry
        super(menu_Kick, self).__init__()
        uic.loadUi('ui/template.ui', self)
        Base.default_stuff(self)
        extra_template.reset(self, self)
        #define buttons
        btn_kick_unknown = self.findChild(QtWidgets.QPushButton, 'btn1')
        btn_kick_ip = self.findChild(QtWidgets.QPushButton, 'btn2')
        btn_back = self.findChild(QtWidgets.QPushButton, 'btn8')
        self.txt_top = self.findChild(QtWidgets.QLabel, 'txttop')

        # Show btns
        btn_back.show()
        btn_kick_unknown.show()
        btn_kick_ip.show()
        self.txt_top.show()

        # Set text on Btns and btm text
        btn_kick_unknown.setText('Kick Unknowns')
        btn_kick_ip.setText('Kick by IP')
        btn_back.setText('Back')
        self.findChild(QtWidgets.QLabel, 'txtbtm').setText('Kick')

        # Define function to do on click
        btn_back.clicked.connect(self.back_func)
        btn_kick_unknown.clicked.connect(self.unknowns)
        btn_kick_ip.clicked.connect(self.byip)
        
        self.restoreGeometry(geometry)

    def back_func(self):
        geometry = self.saveGeometry()
        self.menu = menu_MainPage(geometry)
        self.menu.show()
        self.close()

    def byip(self):
        geometry = self.saveGeometry()
        self.menu = menu_Kick_byip(geometry)
        self.menu.show()
        self.close()
    
    def unknowns(self):
        
        local_ip = get_private_ip()
        ip_set = {local_ip}
        public_ip = get_public_ip()
        if public_ip:
            ip_set.add(public_ip)
        else:
            logger.warning('Failed to get Public IP. Running without.')
        for ip, friend in custom_ips:
            if friend.get('enabled'):
                ip_calc = extra_validate.validate_ip(ip)
                if ip_calc != False:
                    ip_set.add(ip_calc)
                else:
                    logger.warning('Not valid IP or URL: {}'.format(ip))
                    continue

        for ip, friend in friends:
            if friend.get('enabled'):
                ip_set.add(ip)
        time.sleep(1)
        packet_filter = Whitelist(ips=ip_set)
        packet_filter.start()
        for _ in range(10):
            time.sleep(1)
        packet_filter.stop()
        self.txt_top.setText('Kicked unknowns')
    
class menu_Kick_byip(QWidget):
    def __init__(self, geometry):
        super(menu_Kick_byip, self).__init__()
        uic.loadUi('ui/list.ui', self)
        Base.default_stuff(self)
        self.restoreGeometry(geometry)

        # Create list and btns
        self.widget_list = self.findChild(QtWidgets.QListWidget, 'listWidget')#QListWidget()
        self.btn_kick = self.findChild(QtWidgets.QPushButton, 'Kick')#QPushButton()
        self.txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')
        self.btn_back = self.findChild(QtWidgets.QPushButton, 'back')#QPushButton()
        self.findChild(QtWidgets.QPushButton, 'delete_2').hide()

        #self.widget_list.itemClicked.connect(self.item_click)
        self.btn_back.clicked.connect(self.back_func)
        self.btn_kick.clicked.connect(self.kick)

        #change names on buttons
        
        
        collector = IPCollector()
        collector.start()
        for n in range(10):
            self.txt_btm.setText('Collecting IPs, done in {} seconds...'.format(10 - n))
            time.sleep(1)
        collector.stop()
        ip_set = set(collector.ips)
        if len(ip_set) <= 0:
            self.txt_btm.setText('Your\'e lonely my dear friend')

        for ip in ip_set:
            item = QListWidgetItem()
            item.setText(ip)
            item.setSizeHint(QtCore.QSize(340, 40))
            item.setBackground(QtGui.QColor('#bfbfbf'))
            item.setTextAlignment(0x0080 | 0x0004)
            self.widget_list.addItem(item)


        

    def back_func(self):
        geometry = self.saveGeometry()
        self.menu = menu_Kick(geometry)
        self.menu.show()
        self.close()
    def kick(self):
        ip = self.widget_list.currentItem()
        if ip == None:
            ctypes.windll.user32.MessageBoxW(0, "You need to select a IP before you can kick it.", "Kick By IP", 0)
            return
        packet_filter = Blacklist(ips=ip)
        packet_filter.start()
        for _ in range(10):
            time.sleep(1)
        packet_filter.stop()
        QMessageBox.information(self, 'Kick By IP', "Kicked that bastard ;)", QMessageBox.ok)

    def edit_func(self):
        list_name = self.widget_list.currentItem()
        if list_name == None:
            ctypes.windll.user32.MessageBoxW(0, "You need to select a item before you can edit it.", "Custom IP List", 0)
            return
        list_name = list_name.text()
        org_ip, org_item = custom_ips.find(list_name)
        entry = org_item.get('value', org_ip)
        custom_ips.delete(org_ip)
        config.save()

        #ask for name to add
        name, okPressed1 = QInputDialog.getText(self, "Custom IPs Edit","Name:", QLineEdit.Normal, list_name)
        #is there a name and did you press ok?
        if okPressed1 and name != '':
            name = extra_validate.name_in_custom(name) # Returns false if already exists
            if name != False:
                logger.info('Name Valid: {}'.format(name))
                # ask for ip/domain
                ip, okPressed2 = QInputDialog.getText(self, "Custom IPs Add","IP/Domain:", QLineEdit.Normal, entry)
                if okPressed2 and ip != '':
                    #check if ip/domain already exits
                    ip = extra_validate.ip_in_custom(ip)
                    if ip != False:
                        logger.info('IP or Domain not in custom: {}'.format(ip))

                        #check if ip is valid and add to custom ips
                        
                        #validate ip and set it to ip_new to later check if it is a domain or ip
                        ip_new = extra_validate.validate_ip(ip)
                        if ip_new != False:
                            item = {
                                'name': name,
                                'enabled': org_item.get('enabled')
                            }
                            #if its a domain, set value to the domain
                            if ip_new != ip:
                                item['value'] = ip
                            custom_ips.add(ip_new, item)
                            config.save()
                        else:
                            logger.error('{} isn\'t a valid ip/domain. Reverting back to original'.format(ip))
                            custom_ips.add(org_ip, org_item)
                            config.save()

                    else:
                        logger.warning('IP or Domain is already in custom: {}. Reverting back to original'.format(ip))
                        custom_ips.add(org_ip, org_item)
                        config.save()
                else:
                    logger.warning('Nothing entered: {}. Reverting back to original')
                    custom_ips.add(org_ip, org_item)
                    config.save()
            else:
                logger.warning('Name already used: {}. Reverting back to original'.format(name))
                custom_ips.add(org_ip, org_item)
                config.save()
        else:
            logger.warning('Nothing entered: {}. Reverting back to original')
            custom_ips.add(org_ip, org_item)
            config.save()
            
        self.load_items()
  
# Settings menu
class menu_settings(QWidget):
    def __init__(self, geometry):
        self.geometry = geometry
        super(menu_settings, self).__init__()
        uic.loadUi('ui/template.ui', self)
        Base.default_stuff(self)
        extra_template.reset(self, self)
        #define buttons
        btn_token = self.findChild(QtWidgets.QPushButton, 'btn1')
        btn_lists = self.findChild(QtWidgets.QPushButton, 'btn2')
        btn_back = self.findChild(QtWidgets.QPushButton, 'btn8')

        # Show btns
        btn_back.show()
        btn_token.show()
        btn_lists.show()

        # Set text on Btns and btm text
        btn_token.setText('Token')
        btn_lists.setText('Lists')
        btn_back.setText('Back')
        self.findChild(QtWidgets.QLabel, 'txtbtm').setText('Settings')

        # Define function to do on click
        btn_back.clicked.connect(self.back)
        btn_lists.clicked.connect(self.lists)
        btn_token.clicked.connect(self.dialog_token)
        
        self.restoreGeometry(geometry)

    def lists(self):
        geometry = self.saveGeometry()
        self.menu = menu_List(geometry)
        self.menu.show()
        self.close()
    
    def back(self):
        geometry = self.saveGeometry()
        self.menu = menu_MainPage(geometry)
        self.menu.show()
        self.close()
    
    def dialog_token(self):
        global token
        token = config.get('token')
        if token:
            text, ok = QInputDialog.getText(self, 'Input Dialog', 'Enter token:', 0, token)
        else:
            text, ok = QInputDialog.getText(self, 'Input Dialog', 'Enter token:')
        if ok:
            if text:
                logger.debug('token is inputed')
                if extra_tokencheck.check(text, False):
                    config.set('token', text)
                    logger.info('token is valid from input')
                    config.save()
                    token = config.get('token')
                    data.update_cloud_friends()
                else:
                    logger.error('token is invalid from input')
            else:
                logger.warning('text is none')

# List menu menu
class menu_List(QWidget):
    def __init__(self, geometry):
        super(menu_List, self).__init__()
        uic.loadUi('ui/template.ui', self)
        Base.default_stuff(self)
        extra_template.reset(self, self)
        #define buttons
        btn_custom = self.findChild(QtWidgets.QPushButton, 'btn1')
        btn_cloud = self.findChild(QtWidgets.QPushButton, 'btn2')
        btn_blacklist = self.findChild(QtWidgets.QPushButton, 'btn3')
        btn_back = self.findChild(QtWidgets.QPushButton, 'btn8')

        # Show btns
        btn_custom.show()
        btn_cloud.show()
        btn_blacklist.show()
        btn_back.show()

        # Set text on Btns and btm text
        btn_custom.setText('Custom')
        btn_cloud.setText('Cloud')
        btn_blacklist.setText('Blacklist')
        btn_back.setText('Back')
        self.findChild(QtWidgets.QLabel, 'txtbtm').setText('Settings > Lists')

        # Define function to do on click
        btn_custom.clicked.connect(self.custom)
        btn_blacklist.clicked.connect(self.blacklist)
        btn_cloud.clicked.connect(self.cloud)
        btn_back.clicked.connect(self.back)
        
        self.restoreGeometry(geometry)
    def custom(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Custom(geometry)
        self.menu.show()
        self.close()
    
    def blacklist(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Blacklist(geometry)
        self.menu.show()
        self.close()
        
    def cloud(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Cloud(geometry)
        self.menu.show()
        self.close()

    def back(self):
        geometry = self.saveGeometry()
        self.menu = menu_settings(geometry)
        self.menu.show()
        self.close()

class menu_List_Custom(QWidget):
    def __init__(self, geometry):
        super(menu_List_Custom, self).__init__()
        uic.loadUi('ui/template.ui', self)
        Base.default_stuff(self)
        extra_template.reset(self, self)
        #define buttons
        btn_select = self.findChild(QtWidgets.QPushButton, 'btn1')
        btn_add = self.findChild(QtWidgets.QPushButton, 'btn2')
        btn_list = self.findChild(QtWidgets.QPushButton, 'btn3')
        btn_back = self.findChild(QtWidgets.QPushButton, 'btn8')

        # Show btns
        btn_select.show()
        btn_add.show()
        btn_list.show()
        btn_back.show()

        # Set text on Btns and btm text
        btn_select.setText('Select')
        btn_add.setText('Add')
        btn_list.setText('List')
        btn_back.setText('Back')
        self.findChild(QtWidgets.QLabel, 'txtbtm').setText('Settings > Lists > Custom')

        # Define function to do on click
        btn_back.clicked.connect(self.back)
        btn_select.clicked.connect(self.select)
        btn_list.clicked.connect(self.lists)
        btn_add.clicked.connect(self.add)
        
        self.restoreGeometry(geometry)

    def select(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Custom_Select(geometry)
        self.menu.show()
        self.close()

    def lists(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Custom_List(geometry)
        self.menu.show()
        self.close()
			
    def add(self):
        #ask for name to add
        name, okPressed1 = QInputDialog.getText(self, "Custom IPs Add","Name:", QLineEdit.Normal, "")
        #is there a name and did you press ok?
        if okPressed1 and name != '':
            #check if name already exits
            name = extra_validate.name_in_custom(name) # Returns false if already exists
            if name != False:
                logger.info('Name Valid: {}'.format(name))
                # ask for ip/domain
                ip, okPressed2 = QInputDialog.getText(self, "Custom IPs Add","IP/Domain:", QLineEdit.Normal, "")
                if okPressed2 and ip != '':
                    #check if ip/domain already exits
                    ip = extra_validate.ip_in_custom(ip)
                    if ip != False:
                        logger.info('IP or Domain not in custom: {}'.format(ip))
                        #check if ip is valid and add to custom ips
                        #validate ip and set it to ip_new to later check if it is a domain or ip
                        ip_new = extra_validate.validate_ip(ip)
                        if ip_new != False:
                            item = {
                                'name': name,
                                'enabled': True
                            }
                            #if its a domain, set value to the domain
                            if ip_new != ip:
                                item['value'] = ip
                            custom_ips.add(ip_new, item)
                            print(custom_ips)
                            config.save()
                        else:
                            logger.error('{} isn\'t a valid ip/domain'.format(ip))

                    else:
                        logger.warning('IP or Domain is already in custom: {}'.format(ip))
            else:
                logger.warning('Name already used: {}'.format(name))
                
        
    def back(self):
        geometry = self.saveGeometry()
        self.menu = menu_List(geometry)
        self.menu.show()
        self.close()

class menu_List_Custom_Select(QWidget):
    def __init__(self, geometry):
        super(menu_List_Custom_Select, self).__init__()
        uic.loadUi('ui/select.ui', self)
        Base.default_stuff(self)
        
        btn_help = self.findChild(QtWidgets.QPushButton, 'help')
        btn_back = self.findChild(QtWidgets.QPushButton, 'back')
        txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')

        
        btn_back.clicked.connect(self.back_func)
        self.scroll = self.findChild(QtWidgets.QScrollArea, 'scrollArea')
        self.grid = QVBoxLayout()
        
        self.txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')
        
        if len(custom_ips) == 0:
            txt_btm.setText('You havn\'t added any Custom IPs yet')
        else:
            self.Add_select_all()
            for ip, f in custom_ips:
                self.Add(f)
        mygroupbox = QGroupBox()
        mygroupbox.setLayout(self.grid)
        self.scroll.setWidget(mygroupbox)
        #self.setLayout(self.grid)
        
        self.restoreGeometry(geometry)

    def Add(self, item):
        check = QCheckBox('     ' + item.get('name'))
        check.setMinimumHeight(30)
        check.setChecked(item.get('enabled'))
        check.stateChanged.connect(partial(self.enabledisable_func, item, check))
        #check.setAlignment(Qt.AlignCenter)
        check.setStyleSheet("background-color: #bfbfbf;")
        self.grid.addWidget(check)

    def Add_select_all(self):
        check = QCheckBox('     Toggle Everyone, dosnt refresh checkboxes!')
        check.setMinimumHeight(30)
        check.stateChanged.connect(partial(self.select_all, check))
        #check.setAlignment(Qt.AlignCenter)
        check.setStyleSheet("background-color: #bfbfbf;")
        self.grid.addWidget(check)

    def select_all(self,check):
        checked = check.isChecked()
        items = (self.grid.itemAt(i) for i in range(self.grid.count())) 
        for ip, f in custom_ips:
            f['enabled'] = checked
        config.save()

    def enabledisable_func(self, item, check):
        item['enabled'] = check.isChecked()
        config.save()
    def back_func(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Custom(geometry)
        self.menu.show()
        self.close()

class menu_List_Custom_List(QWidget):
    def __init__(self, geometry):
        super(menu_List_Custom_List, self).__init__()
        uic.loadUi('ui/list.ui', self)
        Base.default_stuff(self)

        # Create list and btns
        self.widget_list = self.findChild(QtWidgets.QListWidget, 'listWidget')#QListWidget()
        self.btn_edit = self.findChild(QtWidgets.QPushButton, 'edit')#QPushButton()
        self.btn_delete = self.findChild(QtWidgets.QPushButton, 'delete_2')# QPushButton()
        self.btn_back = self.findChild(QtWidgets.QPushButton, 'back')#QPushButton()
        self.txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')

        #self.widget_list.itemClicked.connect(self.item_click)
        self.btn_delete.clicked.connect(self.delete)
        self.btn_back.clicked.connect(self.back_func)
        self.btn_edit.clicked.connect(self.edit_func)

        self.load_items()
        
        self.restoreGeometry(geometry)

    def load_items(self):
        self.widget_list.clear()
        for ip, f in custom_ips:
            item = QListWidgetItem()
            item.setText(f.get('name'))
            item.setSizeHint(QtCore.QSize(340, 40))
            item.setBackground(QtGui.QColor('#bfbfbf'))
            item.setTextAlignment(0x0080 | 0x0004)
            self.widget_list.addItem(item)
        if len(custom_ips) == 0:
            self.txt_btm.setText('You don\'t have any custom IP\'s to show')
    def back_func(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Custom(geometry)
        self.menu.show()
        self.close()
    def delete(self):
        name = self.widget_list.currentItem()
        if name == None:
            ctypes.windll.user32.MessageBoxW(0, "You need to select a item before you can delete it.", "Custom IP List", 0)
            return
        name = name.text()
        ip, item = custom_ips.find(name)
        custom_ips.delete(ip)
        config.save()
        self.load_items()

    def edit_func(self):
        list_name = self.widget_list.currentItem()
        if list_name == None:
            ctypes.windll.user32.MessageBoxW(0, "You need to select a item before you can edit it.", "Custom IP List", 0)
            return
        list_name = list_name.text()
        org_ip, org_item = custom_ips.find(list_name)
        entry = org_item.get('value', org_ip)
        custom_ips.delete(org_ip)
        config.save()

        #ask for name to add
        name, okPressed1 = QInputDialog.getText(self, "Custom IPs Edit","Name:", QLineEdit.Normal, list_name)
        #is there a name and did you press ok?
        if okPressed1 and name != '':
            #check if name already exits
            name = extra_validate.name_in_custom(name) # Returns false if already exists
            if name != False:
                logger.info('Name Valid: {}'.format(name))
                # ask for ip/domain
                ip, okPressed2 = QInputDialog.getText(self, "Custom IPs Add","IP/Domain:", QLineEdit.Normal, entry)
                if okPressed2 and ip != '':
                    #check if ip/domain already exits
                    ip = extra_validate.ip_in_custom(ip)
                    if ip != False:
                        logger.info('IP or Domain not in custom: {}'.format(ip))
                        #check if ip is valid and add to custom ips

                        #validate ip and set it to ip_new to later check if it is a domain or ip
                        ip_new = extra_validate.validate_ip(ip)
                        if ip_new != False:
                            item = {
                                'name': name,
                                'enabled': org_item.get('enabled')
                            }
                            #if its a domain, set value to the domain
                            if ip_new != ip:
                                item['value'] = ip
                            custom_ips.add(ip_new, item)
                            config.save()
                        else:
                            logger.error('{} isn\'t a valid ip/domain. Reverting back to original'.format(ip))
                            custom_ips.add(org_ip, org_item)
                            config.save()

                    else:
                        logger.warning('IP or Domain is already in custom: {}. Reverting back to original'.format(ip))
                        custom_ips.add(org_ip, org_item)
                        config.save()
                else:
                    logger.warning('Nothing entered: {}. Reverting back to original')
                    custom_ips.add(org_ip, org_item)
                    config.save()
            else:
                logger.warning('Name already used: {}. Reverting back to original'.format(name))
                custom_ips.add(org_ip, org_item)
                config.save()
        else:
            logger.warning('Nothing entered: {}. Reverting back to original')
            custom_ips.add(org_ip, org_item)
            config.save()
            
        self.load_items()
        
class menu_List_Blacklist(QWidget):
    def __init__(self, geometry):
        super(menu_List_Blacklist, self).__init__()
        uic.loadUi('ui/template.ui', self)
        Base.default_stuff(self)
        extra_template.reset(self, self)
        #define buttons
        btn_select = self.findChild(QtWidgets.QPushButton, 'btn1')
        btn_add = self.findChild(QtWidgets.QPushButton, 'btn2')
        btn_list = self.findChild(QtWidgets.QPushButton, 'btn3')
        btn_back = self.findChild(QtWidgets.QPushButton, 'btn8')

        # Show btns
        btn_select.show()
        btn_add.show()
        btn_list.show()
        btn_back.show()

        # Set text on Btns and btm text
        btn_select.setText('Select')
        btn_add.setText('Add')
        btn_list.setText('List')
        btn_back.setText('Back')
        self.findChild(QtWidgets.QLabel, 'txtbtm').setText('Settings > Lists > Blacklist')

        # Define function to do on click
        btn_back.clicked.connect(self.back)
        btn_select.clicked.connect(self.select)
        btn_list.clicked.connect(self.lists)
        btn_add.clicked.connect(self.add)
        
        self.restoreGeometry(geometry)

    def select(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Blacklist_Select(geometry)
        self.menu.show()
        self.close()

    def lists(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Blacklist_List(geometry)
        self.menu.show()
        self.close()
			
    def add(self):
        #ask for name to add
        name, okPressed1 = QInputDialog.getText(self, "Blacklisted IPs Add","Name:", QLineEdit.Normal, "")
        #is there a name and did you press ok?
        if okPressed1 and name != '':
            #check if name already exits
            name = extra_validate.name_in_blacklist(name) # Returns false if already exists
            if name != False:
                logger.info('Name Valid: {}'.format(name))
                # ask for ip/domain
                ip, okPressed2 = QInputDialog.getText(self, "Blacklisted IPs Add","IP/Domain:", QLineEdit.Normal, "")
                if okPressed2 and ip != '':
                    #check if ip/domain already exits
                    ip = extra_validate.ip_in_blacklist(ip)
                    if ip != False:
                        logger.info('IP or Domain not in Blacklist: {}'.format(ip))
                        #check if ip is valid and add to Blacklisted ips

                        #validate ip and set it to ip_new to later check if it is a domain or ip
                        ip_new = extra_validate.validate_ip(ip)
                        if ip_new != False:
                            item = {
                                'name': name,
                                'enabled': True
                            }
                            #if its a domain, set value to the domain
                            if ip_new != ip:
                                item['value'] = ip
                            blacklist.add(ip_new, item)
                            config.save()
                        else:
                            logger.error('{} isn\'t a valid ip/domain'.format(ip))

                    else:
                        logger.warning('IP or Domain is already in blacklist: {}'.format(ip))
            else:
                logger.warning('Name already used: {}'.format(name))
                
        
    def back(self):
        geometry = self.saveGeometry()
        self.menu = menu_List(geometry)
        self.menu.show()
        self.close()

class menu_List_Blacklist_Select(QWidget):
    def __init__(self, geometry):
        super(menu_List_Blacklist_Select, self).__init__()
        uic.loadUi('ui/select.ui', self)
        Base.default_stuff(self)
        
        btn_help = self.findChild(QtWidgets.QPushButton, 'help')
        btn_back = self.findChild(QtWidgets.QPushButton, 'back')
        txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')

        
        btn_back.clicked.connect(self.back_func)
        self.scroll = self.findChild(QtWidgets.QScrollArea, 'scrollArea')
        self.grid = QVBoxLayout()
        
        self.txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')
        
        if len(blacklist) == 0:
            txt_btm.setText('You havn\'t added any Blacklisted IPs yet')
        else:
            self.Add_select_all()
            for ip, f in blacklist:
                self.Add(f)
        mygroupbox = QGroupBox()
        mygroupbox.setLayout(self.grid)
        self.scroll.setWidget(mygroupbox)
        #self.setLayout(self.grid)
        
        self.restoreGeometry(geometry)

    def Add(self, item):
        check = QCheckBox('     ' + item.get('name'))
        check.setMinimumHeight(30)
        check.setChecked(item.get('enabled'))
        check.stateChanged.connect(partial(self.enabledisable_func, item, check))
        #check.setAlignment(Qt.AlignCenter)
        check.setStyleSheet("background-color: #bfbfbf;")
        self.grid.addWidget(check)

    def Add_select_all(self):
        check = QCheckBox('     Toggle Everyone, dosnt refresh checkboxes!')
        check.setMinimumHeight(30)
        check.stateChanged.connect(partial(self.select_all, check))
        #check.setAlignment(Qt.AlignCenter)
        check.setStyleSheet("background-color: #bfbfbf;")
        self.grid.addWidget(check)

    def select_all(self,check):
        checked = check.isChecked()
        items = (self.grid.itemAt(i) for i in range(self.grid.count())) 
        for ip, f in blacklist:
            f['enabled'] = checked
        config.save()

    def enabledisable_func(self, item, check):
        item['enabled'] = check.isChecked()
        config.save()
    def back_func(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Blacklist(geometry)
        self.menu.show()
        self.close()

class menu_List_Blacklist_List(QWidget):
    def __init__(self, geometry):
        super(menu_List_Blacklist_List, self).__init__()
        uic.loadUi('ui/list.ui', self)
        Base.default_stuff(self)

        # Create list and btns
        self.widget_list = self.findChild(QtWidgets.QListWidget, 'listWidget')#QListWidget()
        self.btn_edit = self.findChild(QtWidgets.QPushButton, 'edit')#QPushButton()
        self.btn_delete = self.findChild(QtWidgets.QPushButton, 'delete_2')# QPushButton()
        self.btn_back = self.findChild(QtWidgets.QPushButton, 'back')#QPushButton()
        self.txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')

        #self.widget_list.itemClicked.connect(self.item_click)
        self.btn_delete.clicked.connect(self.delete)
        self.btn_back.clicked.connect(self.back_func)
        self.btn_edit.clicked.connect(self.edit_func)

        self.load_items()
        
        self.restoreGeometry(geometry)

    def load_items(self):
        self.widget_list.clear()
        for ip, f in blacklist:
            item = QListWidgetItem()
            item.setText(f.get('name'))
            item.setSizeHint(QtCore.QSize(340, 40))
            item.setBackground(QtGui.QColor('#bfbfbf'))
            item.setTextAlignment(0x0080 | 0x0004)
            self.widget_list.addItem(item)
        if len(custom_ips) == 0:
            self.txt_btm.setText('You don\'t have any blacklisted IP\'s to show')
    def back_func(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Blacklist(geometry)
        self.menu.show()
        self.close()
    def delete(self):
        name = self.widget_list.currentItem()
        if name == None:
            ctypes.windll.user32.MessageBoxW(0, "You need to select a item before you can delete it.", "Blacklisted IP List", 0)
            return
        name = name.text()
        ip, item = blacklist.find(name)
        blacklist.delete(ip)
        config.save()
        self.load_items()

    def edit_func(self):
        list_name = self.widget_list.currentItem()
        if list_name == None:
            ctypes.windll.user32.MessageBoxW(0, "You need to select a item before you can edit it.", "Blacklisted IP List", 0)
            return
        list_name = list_name.text()
        org_ip, org_item = blacklist.find(list_name)
        entry = org_item.get('value', org_ip)
        blacklist.delete(org_ip)
        config.save()

        #ask for name to add
        name, okPressed1 = QInputDialog.getText(self, "Blacklisted IPs Edit","Name:", QLineEdit.Normal, list_name)
        #is there a name and did you press ok?
        if okPressed1 and name != '':
            #check if name already exits
            name = extra_validate.name_in_blacklist(name) # Returns false if already exists
            if name != False:
                logger.info('Name Valid: {}'.format(name))
                # ask for ip/domain
                ip, okPressed2 = QInputDialog.getText(self, "Blacklisted IPs Add","IP/Domain:", QLineEdit.Normal, entry)
                if okPressed2 and ip != '':
                    #check if ip/domain already exits
                    ip = extra_validate.ip_in_blacklist(ip)
                    if ip != False:
                        logger.info('IP or Domain not in blacklist: {}'.format(ip))
                        #check if ip is valid and add to blacklist ips
                        #validate ip and set it to ip_new to later check if it is a domain or ip
                        ip_new = extra_validate.validate_ip(ip)
                        if ip_new != False:
                            item = {
                                'name': name,
                                'enabled': org_item.get('enabled')
                            }
                            #if its a domain, set value to the domain
                            if ip_new != ip:
                                item['value'] = ip
                            blacklist.add(ip_new, item)
                            config.save()
                        else:
                            logger.error('{} isn\'t a valid ip/domain. Reverting back to original'.format(ip))
                            blacklist.add(org_ip, org_item)
                            config.save()

                    else:
                        logger.warning('IP or Domain is already in blacklist: {}. Reverting back to original'.format(ip))
                        blacklist.add(org_ip, org_item)
                        config.save()
                else:
                    logger.warning('Nothing entered: {}. Reverting back to original')
                    blacklist.add(org_ip, org_item)
                    config.save()
            else:
                logger.warning('Name already used: {}. Reverting back to original'.format(name))
                blacklist.add(org_ip, org_item)
                config.save()
        else:
            logger.warning('Nothing entered: {}. Reverting back to original')
            blacklist.add(org_ip, org_item)
            config.save()
            
        self.load_items()
        
class menu_List_Cloud(QWidget):
    def __init__(self, geometry):
        super(menu_List_Cloud, self).__init__()
        uic.loadUi('ui/template.ui', self)
        Base.default_stuff(self)
        extra_template.reset(self, self)
        #define buttons
        btn_select = self.findChild(QtWidgets.QPushButton, 'btn1')
        btn_permission = self.findChild(QtWidgets.QPushButton, 'btn2')
        btn_back = self.findChild(QtWidgets.QPushButton, 'btn8')

        # Show btns
        btn_select.show()
        btn_permission.show()
        btn_back.show()

        # Set text on Btns and btm text
        btn_select.setText('Select')
        btn_permission.setText('Permission')
        btn_back.setText('Back')
        self.findChild(QtWidgets.QLabel, 'txtbtm').setText('Settings > Lists > Cloud')

        # Define function to do on click
        btn_back.clicked.connect(self.back)
        btn_select.clicked.connect(self.select)
        btn_permission.clicked.connect(self.permission)
        
        self.restoreGeometry(geometry)

    def select(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Cloud_Select(geometry)
        self.menu.show()
        self.close()

    def permission(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Cloud_Permission(geometry)
        self.menu.show()
        self.close()
        
    def back(self):
        geometry = self.saveGeometry()
        self.menu = menu_List(geometry)
        self.menu.show()
        self.close()

class menu_List_Cloud_Select(QWidget):
    def __init__(self, geometry):
        super(menu_List_Cloud_Select, self).__init__()
        uic.loadUi('ui/select.ui', self)
        Base.default_stuff(self)
        
        btn_help = self.findChild(QtWidgets.QPushButton, 'help')
        btn_back = self.findChild(QtWidgets.QPushButton, 'back')
        txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')

        
        btn_back.clicked.connect(self.back_func)
        self.scroll = self.findChild(QtWidgets.QScrollArea, 'scrollArea')
        self.grid = QVBoxLayout()
        
        self.txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')
        txt_btm.setText('Lists > Cloud > Select')
        if len(friends) == 0:
            txt_btm.setText('You don\'t have any friends')
        else:
            self.Add_select_all()
            for ip, f in friends:
                self.Add(f)
        
        mygroupbox = QGroupBox()
        mygroupbox.setLayout(self.grid)
        self.scroll.setWidget(mygroupbox)
        #self.setLayout(self.grid)
        
        self.restoreGeometry(geometry)

    def Add(self, item):
        check = QCheckBox('     ' + item.get('name'))
        check.setMinimumHeight(30)
        check.setChecked(item.get('enabled'))
        check.stateChanged.connect(partial(self.enabledisable_func, item, check))
        #check.setAlignment(Qt.AlignCenter)
        check.setStyleSheet("background-color: #bfbfbf;")
        self.grid.addWidget(check)
    def Add_select_all(self):
        check = QCheckBox('     Toggle Everyone, dosnt refresh checkboxes!')
        check.setMinimumHeight(30)
        check.stateChanged.connect(partial(self.select_all, check))
        #check.setAlignment(Qt.AlignCenter)
        check.setStyleSheet("background-color: #bfbfbf;")
        self.grid.addWidget(check)
    def select_all(self,check):
        checked = check.isChecked()
        items = (self.grid.itemAt(i) for i in range(self.grid.count())) 
        for ip, f in friends:
            f['enabled'] = checked
        config.save()
    def enabledisable_func(self, item, check):
        item['enabled'] = check.isChecked()
        config.save()
    def back_func(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Cloud(geometry)
        self.menu.show()
        self.close()
  
class menu_List_Cloud_Permission(QWidget):
    def __init__(self, geometry):
        super(menu_List_Cloud_Permission, self).__init__()
        uic.loadUi('ui/template.ui', self)
        Base.default_stuff(self)
        extra_template.reset(self, self)
        #define buttons
        btn_revoke = self.findChild(QtWidgets.QPushButton, 'btn1')
        btn_request = self.findChild(QtWidgets.QPushButton, 'btn2')
        btn_pending = self.findChild(QtWidgets.QPushButton, 'btn3')
        btn_back = self.findChild(QtWidgets.QPushButton, 'btn8')

        btn_back.show()
        btn_pending.show()
        btn_request.show()
        btn_revoke.show()

        # Set text on Btns and btm text
        btn_revoke.setText('Revoke\n permission')
        btn_request.setText('Request\n permission')
        btn_pending.setText('Pending\n requests')
        btn_back.setText('Back')
        self.findChild(QtWidgets.QLabel, 'txtbtm').setText('Lists > Cloud > Permission')

        # Define function to do on click
        btn_back.clicked.connect(self.back)
        btn_revoke.clicked.connect(self.revoke)
        btn_request.clicked.connect(self.request)
        btn_pending.clicked.connect(self.pending)
        #btn_pending.clicked.connect(self.permission)
        
        self.restoreGeometry(geometry)

    def revoke(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Cloud_Permission_Revoke(geometry)
        self.menu.show()
        self.close()
        
    def request(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Cloud_Permission_Request(geometry)
        self.menu.show()
        self.close()
        
    def pending(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Cloud_Permission_Pending(geometry)
        self.menu.show()
        self.close()

    def back(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Cloud(geometry)
        self.menu.show()
        self.close()

class menu_List_Cloud_Permission_Revoke(QWidget):
    def __init__(self, geometry):
        super(menu_List_Cloud_Permission_Revoke, self).__init__()
        uic.loadUi('ui/list.ui', self)
        Base.default_stuff(self)

        # Create list and btns
        self.widget_list = self.findChild(QtWidgets.QListWidget, 'listWidget')#QListWidget()
        self.btn_revoke = self.findChild(QtWidgets.QPushButton, 'edit')#QPushButton()
        self.findChild(QtWidgets.QPushButton, 'delete_2').hide()
        self.btn_back = self.findChild(QtWidgets.QPushButton, 'back')#QPushButton()
        self.txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')

        #self.widget_list.itemClicked.connect(self.item_click)
        #self.btn_delete.clicked.connect(self.delete)
        self.btn_back.clicked.connect(self.back_func)
        self.btn_revoke.clicked.connect(self.revoke)

        self.btn_revoke.setText('Revoke')
        
        self.load_items()
        
        self.restoreGeometry(geometry)

    def load_items(self):
        self.widget_list.clear()
        logger.debug('Load items token is {}'.format(token))
        cloud = networkmanager.Cloud(token)
        allowed = cloud.get_allowed()
        if allowed:
            for f in allowed:
                item = QListWidgetItem()
                item.setText(f.get('name'))
                item.setSizeHint(QtCore.QSize(340, 40))
                item.setBackground(QtGui.QColor('#bfbfbf'))
                item.setTextAlignment(0x0080 | 0x0004)
                self.widget_list.addItem(item)
        else:
            self.txt_btm.setText('You havn\'t accepted any permissions')
    def back_func(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Cloud_Permission(geometry)
        self.menu.show()
        self.close()
    def revoke(self):
        name = self.widget_list.currentItem()
        if name == None:
            ctypes.windll.user32.MessageBoxW(0, "You need to select a friend before you can revoke their permission.", "Revoke Permission", 0)
            return
        name = name.text()
        code, msg = cloud.revoke(name)
        if code:
            ctypes.windll.user32.MessageBoxW(0, 'Revoked {}'.format(name), "Revoke Permission", 0)
            logger.info('Revoked {}'.format(name))
        else:
            ctypes.windll.user32.MessageBoxW(0, 'Error: Couldnt revoke player', "Revoke Permission", 0)
            logger.error('Error: Couldnt revoke player')
        self.load_items()

class menu_List_Cloud_Permission_Pending(QWidget):
    def __init__(self, geometry):
        super(menu_List_Cloud_Permission_Pending, self).__init__()
        uic.loadUi('ui/list.ui', self)
        Base.default_stuff(self)

        # Create list and btns
        self.widget_list = self.findChild(QtWidgets.QListWidget, 'listWidget')#QListWidget()
        self.btn_decline = self.findChild(QtWidgets.QPushButton, 'edit')#QPushButton()
        self.btn_accept = self.findChild(QtWidgets.QPushButton, 'delete_2')
        self.btn_back = self.findChild(QtWidgets.QPushButton, 'back')#QPushButton()
        self.txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')

        #self.widget_list.itemClicked.connect(self.item_click)
        #self.btn_delete.clicked.connect(self.delete)
        self.btn_back.clicked.connect(self.back_func)
        self.btn_accept.clicked.connect(self.accept)
        self.btn_decline.clicked.connect(self.decline)

        self.btn_accept.setText('Accept')
        self.btn_decline.setText('Decline')
        
        self.load_items()
        
        self.restoreGeometry(geometry)

    def load_items(self):
        self.widget_list.clear()
        cloud = networkmanager.Cloud(token)
        friends = cloud.get_pending()
        if friends:
            for f in friends:
                item = QListWidgetItem()
                item.setText(f.get('name'))
                item.setSizeHint(QtCore.QSize(340, 40))
                item.setBackground(QtGui.QColor('#bfbfbf'))
                item.setTextAlignment(0x0080 | 0x0004)
                self.widget_list.addItem(item)
        else:
            self.txt_btm.setText('You dont\'t have any pending permissions')
    def back_func(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Cloud_Permission(geometry)
        self.menu.show()
        self.close()

    def accept(self):
        name = self.widget_list.currentItem()
        if name == None:
            ctypes.windll.user32.MessageBoxW(0, "You need to select a friend before you can accept their request.", "Revoke Permission", 0)
            return
        name = name.text()
        result, msg = cloud.accept(name)
        if result:
            ctypes.windll.user32.MessageBoxW(0, "Accepted.", "Pending requests", 0)
        else:
            ctypes.windll.user32.MessageBoxW(0, '{}'.format(msg), "Pending requests", 0)
            logger.error('{}'.format(msg))
        self.load_items()
        
    def decline(self):
        name = self.widget_list.currentItem()
        if name == None:
            ctypes.windll.user32.MessageBoxW(0, "You need to select a friend before you can decline their request.", "Revoke Permission", 0)
            return
        name = name.text()
        result, msg = cloud.revoke(name)
        if result:
            ctypes.windll.user32.MessageBoxW(0, "Declined.", "Pending requests", 0)
        else:
            ctypes.windll.user32.MessageBoxW(0, '{}'.format(msg), "Pending requests", 0)
            logger.error('{}'.format(msg))
        self.load_items()
       
class menu_List_Cloud_Permission_Request(QWidget):
    def __init__(self, geometry):
        super(menu_List_Cloud_Permission_Request, self).__init__()
        uic.loadUi('ui/list.ui', self)
        Base.default_stuff(self)

        # Create list and btns
        self.widget_list = self.findChild(QtWidgets.QListWidget, 'listWidget')#QListWidget()
        self.btn_request = self.findChild(QtWidgets.QPushButton, 'edit')#QPushButton()
        self.findChild(QtWidgets.QPushButton, 'delete_2').hide()
        self.btn_back = self.findChild(QtWidgets.QPushButton, 'back')#QPushButton()
        self.txt_btm = self.findChild(QtWidgets.QLabel, 'txtbtm')

        #self.widget_list.itemClicked.connect(self.item_click)
        #self.btn_delete.clicked.connect(self.delete)
        self.btn_back.clicked.connect(self.back_func)
        #self.btn_accept.clicked.connect(self.accept)
        self.btn_request.clicked.connect(self.request)

        self.btn_request.setText('Request')
        
        self.load_items()
        
        self.restoreGeometry(geometry)

    def load_items(self):
        self.widget_list.clear()
        cloud = networkmanager.Cloud(token)
        friends = cloud.get_all()
        if friends:
            for f in friends:
                item = QListWidgetItem()
                item.setText(f.get('name'))
                item.setSizeHint(QtCore.QSize(340, 40))
                item.setBackground(QtGui.QColor('#bfbfbf'))
                item.setTextAlignment(0x0080 | 0x0004)
                self.widget_list.addItem(item)
        else:
            self.txt_btm.setText('There\'s no one to request from')
    def back_func(self):
        geometry = self.saveGeometry()
        self.menu = menu_List_Cloud_Permission(geometry)
        self.menu.show()
        self.close()

    def request(self):
        name = self.widget_list.currentItem()
        if name == None:
            ctypes.windll.user32.MessageBoxW(0, "You need to select a friend before you can send theim a request.", "Request Permission", 0)
            return
        name = name.text()
        result, msg = cloud.request(name)
        if result:
            ctypes.windll.user32.MessageBoxW(0, "Request sent.", "Request Permission", 0)
        else:
            ctypes.windll.user32.MessageBoxW(0, '{}'.format(msg), "Request Permission Error", 0)
            logger.error('{}'.format(msg))
        self.load_items()

#-----------------------#
#        Threads        #
#-----------------------#

def get_public_ip():
    public_ip = networkmanager.Cloud().get_ip()
    if public_ip:
        logger.info('Got a public IP')
        return public_ip
    else:
        logger.warning('Failed to get public IP')
        return False


def get_private_ip():
    soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    soc.connect(("8.8.8.8", 80))
    local_ip = soc.getsockname()[0]
    soc.close()
    return local_ip

class thread_Solo(QtCore.QThread):

    started = QtCore.pyqtSignal()
    def __init__(self):
        QtCore.QThread.__init__(self)
        
 
    def __del__(self):
        logger.debug('solo thread closing down')
        self.doContinue = False
        self.wait()

    def stop(self):
        logger.debug('solo thread closing down')
        self.doContinue = False
        self.wait()


    def run(self):
        self.doContinue = True
        time.sleep(1)
        self.started.emit()
        try:
            while self.doContinue:
                whitelist = Whitelist(ips=[])
                whitelist.start()
                logger.debug("solo running")
                for _ in range(10):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
                whitelist.stop()
                for _ in range(15):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
        finally:
            try:
                whitelist.stop()
            except:
                logger.info('Tried stopped whitelist before ')
            logger.debug("stop")
        logger.debug("thread is done")

class thread_Whitelist(QtCore.QThread):

    started = QtCore.pyqtSignal()
    def __init__(self):
        QtCore.QThread.__init__(self)
        
 
    def __del__(self):
        logger.debug('Whitelist thread closing down')
        self.doContinue = False
        self.wait()

    def stop(self):
        logger.debug('Whitelist thread closing down')
        self.doContinue = False
        self.wait()


    def run(self):
        #blacklist = data.CustomList('blacklist')
        #custom_ips = data.CustomList('custom_ips')
        #friends = data.CustomList('friends')
        self.doContinue = True
        local_ip = get_private_ip()
        ip_set = {local_ip}
        public_ip = get_public_ip()
        if public_ip:
            ip_set.add(public_ip)
        else:
            logger.warning('Failed to get Public IP. Running without.')

        for ip, friend in custom_ips:
            if friend.get('enabled'):
                ip_calc = extra_validate.validate_ip(ip)
                if ip_calc != False:
                    ip_set.add(ip_calc)
                else:
                    logger.warning('Not valid IP or URL: {}'.format(ip))
                    continue

        for ip, friend in friends:
            if friend.get('enabled'):
                ip_set.add(ip)

        logger.info('Starting whitelisted session with {} IPs'.format(len(ip_set)))
        
        self.started.emit()
        try:
            while self.doContinue:
                packet_filter = Whitelist(ips=ip_set)
                packet_filter.start()
                for _ in range(10):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
                packet_filter.stop()
                for _ in range(15):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
        finally:
            logger.debug("stop")
        logger.debug("thread is done")

class thread_Blacklist(QtCore.QThread):

    started = QtCore.pyqtSignal()
    def __init__(self):
        QtCore.QThread.__init__(self)
        
 
    def __del__(self):
        logger.debug('Blacklist thread closing down')
        self.doContinue = False
        self.wait()

    def stop(self):
        logger.debug('Blacklist thread closing down')
        self.doContinue = False
        self.wait()


    def run(self):
        #blacklist = data.CustomList('blacklist')
        self.doContinue = True
        ip_set = set()
        for ip, item in blacklist:
            if item.get('enabled'):
                ip = extra_validate.validate_ip(item.get('ip'))
                if ip != False:
                    ip_set.add(ip)
                else:
                    logger.warning('Not valid IP or URL: {}'.format(ip))
                    continue
        logger.info('Starting blacklisted session with {} IPs'.format(len(ip_set)))

        
        self.started.emit()
        try:
            while self.doContinue:
                packet_filter = Blacklist(ips=ip_set)
                packet_filter.start()
                for _ in range(10):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
                packet_filter.stop()
                for _ in range(15):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
        finally:
            logger.debug("stop")
        logger.debug("thread is done")

class thread_AutoWhitelist(QtCore.QThread):

    started = QtCore.pyqtSignal()
    def __init__(self, top_txt):
        QtCore.QThread.__init__(self)
        self.top_txt = top_txt
        
 
    def __del__(self):
        logger.debug('Auto Whitelist thread closing down')
        self.doContinue = False
        self.wait()

    def stop(self):
        logger.debug('Auto Whitelist thread closing down')
        self.doContinue = False
        self.wait()


    def run(self):
        logger.debug(self.top_txt)
        self.doContinue = True
        logger.info('Starting auto whitelisted session')
        collector = IPCollector()
        logger.info('Starting to collect IPs')
        collector.start()
        self.top_txt.setStyleSheet('color: aqua')
        for n in range(10):
            if self.doContinue == False:
                return
            self.top_txt.setText('Collecting IPs, done in {} seconds...'.format(10 - n))
            time.sleep(1)
        collector.stop()
        ip_set = set(collector.ips)
        self.found_ips = collector.ips
        logger.debug(self.found_ips)
        self.top_txt.setStyleSheet('color: yellow')
        self.top_txt.setText('Done!\nCollected {} IPs'.format(len(ip_set)))
        logger.info('Collected {} IPs'.format(len(ip_set)))
        local_ip = get_private_ip()
        ip_set.add(local_ip)
        public_ip = get_public_ip()
        if public_ip:
            ip_set.add(public_ip)
        else:
            logger.info('Failed to get Public IP. Running without.')

        for ip, friend in custom_ips:
            if friend.get('enabled'):
                ip_calc = extra_validate.validate_ip(ip)
                if ip_calc != False:
                    ip_set.add(ip_calc)
                else:
                    logger.warning('Not valid IP or URL: {}'.format(ip))
                    continue

        for ip, friend in friends:
            if friend.get('enabled'):
                ip_set.add(ip)

        logger.info('Starting whitelisted session with {} IPs'.format(len(ip_set)))
        self.doContinue = True
        self.started.emit()
        try:
            while self.doContinue:
                packet_filter = Whitelist(ips=ip_set)
                packet_filter.start()
                for _ in range(10):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
                packet_filter.stop()
                for _ in range(15):
                    if self.doContinue == False:
                        return
                    time.sleep(1)
        finally:
            logger.debug("stop")
        logger.debug("thread is done")

if __name__ == '__main__':
    #add support to freezing with multithreading
    freeze_support()
    is_admin = ctypes.windll.shell32.IsUserAnAdmin() != 0
    if not is_admin:
        ctypes.windll.user32.MessageBoxW(None, "Eris isnt started in GODMODE aka administrator xD", "I wanna be a god again... plz", 0x0)
        logger.warning('Please start Eris again in adminstratormode')
        sys.exit()
    config = data.ConfigData(data.file_name)
    if config.get('debug'):
        logger.setLevel(logging.DEBUG)
        ch.setLevel(logging.DEBUG)
        fh.setLevel(logging.DEBUG)
        logger.info('debug mode')
    try:
        blacklist = data.CustomList('blacklist')
        custom_ips = data.CustomList('custom_ips')
        friends = data.CustomList('friends')
        logger.info('Custom lists added correctly')
        
    except data.MigrationRequired:
        data.migrate_to_dict()
        time.sleep(5)
        exit(0)
    
    cloud = networkmanager.Cloud()
    logger.info('Checking connections.')
    if cloud.check_connection():
        latest_version = cloud.version()
        #version = version.get('version', None) if version else None
        #print('hi' + parse_version('0.1.0'))
        if parse_version(version) < parse_version(str(latest_version)):
            reps = ctypes.windll.user32.MessageBoxW(0, "You're on version {} and the latest version is {}. Do you want to download the latest version?".format(version, latest_version), "Update found!", 4)
            if reps == 6:
                webbrowser.open('https://gitlab.com/halpcrew/eris/-/releases')
                sys.exit()
        token = config.get('token')
        if token:
            data.update_cloud_friends()
            cloud.token = token
            if cloud.check_token():
                ipsyncer = IPSyncer(token, Event())
                ipsyncer.start()
                logger.info('Starting IP syncer.')
    try:
        logger.info('Starting Eris on version {}'.format(str(version)))
        app = QtWidgets.QApplication(sys.argv)
        app.setStyle('Fusion')
        app.setWindowIcon(QtGui.QIcon('img/eris.png'))
        window = Base()
        app.exec_()
    except:
        error = sys.exc_info()#[0]
        errorbox = QMessageBox()
        errorbox.setText('Something went wrong, and Eris focking crashed m8!')
        errorbox.setInformativeText('Press show details to see what went wrong lol')
        errorbox.setDetailedText('Eris crashed with error {} \n \n More details: \n {} \n \n Process: \n {}'.format(error[0], error[1], error[2]))
        errorbox.setIcon(3)
        errorbox.exec()
        logger.error('Eris Crashed with error: {}'.format(error))
        pass
    finally:
        if token:
            ipsyncer.stop()
    logger.info('Shuting down app')
    logger.info('Done! Bye')
    logger.info('-------------------------------')
    sys.exit()